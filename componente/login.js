import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat'

export default class login extends React.Component {

  render() {
    return (
      <View style={styles.container}>
      <Text style={{color: 'white', fontSize: 60,fontWeight: 'bold'}}>GasoilChat!</Text>
      <TextInput
        style={{padding: 10 ,
          height: 40,
          width: 320,
          color: 'white',
          marginTop: 30}}
        placeholderTextColor='white'
        underlineColorAndroid='white'
        placeholder="Correo"
  
      />
      <TextInput
      style={{padding: 10 ,
        height: 40,
        width: 320,
        color: 'white',
        marginTop: 30}}
      placeholderTextColor='white'
      underlineColorAndroid='white'
      placeholder="Contraseña"
      secureTextEntry={true}
      />
      <TouchableOpacity onPress={() => Actions.Index()}  style={styles.button}>
         <Text style={{  color: '#320059'}}>Entra</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => Actions.Register()}  style={styles.buttonRegister}>
         <Text style={{  color: 'white'}}>Registrate</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1DD84D',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    elevation: 3,
    height: 40,
    width: 320, 
    marginTop: 30,
    backgroundColor: 'white',
        
  },
  buttonRegister: {
    borderColor: 'white',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    elevation: 3,
    height: 40,
    width: 320, 
    marginTop: 40,
    backgroundColor: '#1DD84D',
        
  }
});
