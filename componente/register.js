import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat'

export default class register extends React.Component {

  render() {
    return (
      <View style={styles.container}>
      <Text style={{color: 'white', fontSize: 60,fontWeight: 'bold'}}>Registrate!</Text>
      <TextInput
        style={{padding: 10 ,
          height: 40,
          width: 320,
          color: 'white',
          marginTop: 30}}
        placeholderTextColor='white'
        underlineColorAndroid='white'
        placeholder="Correo"
  
      />
      <TextInput
      style={{padding: 10 ,
        height: 40,
        width: 320,
        color: 'white',
        marginTop: 30}}
      placeholderTextColor='white'
      underlineColorAndroid='white'
      placeholder="Contraseña"
      secureTextEntry={true}
      />
      <TouchableOpacity onPress={() => Actions.Index()}  style={styles.button}>
         <Text style={{  color: '#320059'}}>Registrate</Text>
        </TouchableOpacity>
        <Text style={{color: 'white', fontSize: 12}}>*El correo no tiene que existir todo se queda anonimo*</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1DD84D',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    elevation: 3,
    height: 40,
    width: 320, 
    marginTop: 30,
    backgroundColor: 'white',
        
  },
 
});
