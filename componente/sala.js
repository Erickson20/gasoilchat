import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat'
import SalaBox from './SalaBox';

export default class sala extends React.Component {

  render() {
    return (
      <View style={styles.container}>
      <Text style={{color: 'white', fontSize: 30,fontWeight: 'bold' , textAlign: 'center', marginTop: 20}}>Salas</Text>
      <ScrollView style={{ flex: 1, backgroundColor: '#1DD84D', padding: 5}}>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
        <SalaBox/>
      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1DD84D',
    
    
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    elevation: 3,
    height: 40,
    width: 320, 
    marginTop: 30,
    backgroundColor: 'white',
        
  },
 
});
